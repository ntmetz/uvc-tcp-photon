# TCP_Test

An example program that uses the UVC_Library to send an image from a usb camera over a TCP connection

## Source Directories

### ```/src``` folder

This is the source directory for the example project, everything inside this directory will be built into the executable. It contains only TCP_Test.cpp

If your application contains multiple files, they should all be included in the `src` folder.

### ```/lib``` folder

This is the folder for external libraries. Each folder inside this one must have the same structure as a particle project(a top level directory with an src directory), everything inside lib/*/src will be built into the project along with being part of the include path in particle workbench.

## Compiling your project

To compile the project you must have particle workbench installed(or have particle workbench installed somewhere else). To install workbench, visit [the particle site](https://www.particle.io/workbench/).

Once that is installed, run the `Particle: Compile application(local)` task in vscode/particle workbench, this compiles the elf file into the `/target` directory
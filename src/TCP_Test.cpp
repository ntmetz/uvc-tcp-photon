// Test Application for getting an image from a UVC device and sending it over TCP to a server 
//
// On linux, run `stdbuf -o0 netcat -l -p port_number | stdbuf -i0 -o0 cat > image.jpeg` to receive 
// the image, replacing port_number with the port you want to send the image over

#include "application.h"
#include "UVC_Library.h"

int led_set(String cycles);
int take_picture(String args);

UVC_Library* uvc;
uvc_framebuffer_t framebuffer_0;
uint8_t framebuffer[15000];

// TCP Server configuration
TCPClient client;
const IPAddress ServerIP(127, 0, 0, 1);
const uint16_t ServerPort = 2222;

volatile int g_frames_captured = 0;

void setup() {
  // Setup the LED pin and initialize the UVC_Library and framebuffer
  pinMode(A5, OUTPUT);
  digitalWrite(A5, LOW);
  
  uvc = new UVC_Library();

  framebuffer_0.max_size              = sizeof(framebuffer);
  framebuffer_0.p_framebuffer         = framebuffer;
  framebuffer_0.buffer_full_semaphore = FRAMEBUFFER_READY;
  framebuffer_0.size                  = 0;

  uvc->begin();

  // Add functions/variables to the particle cloud service
  Particle.function("set_led_cycles", led_set);
  Particle.function("take_picture", take_picture);
  Particle.variable("framebuffer_size", framebuffer_0.size);
}

int led_timer = 0;

void loop() {
  // process() must be called periodically to update the uvc state machine
  uvc->process();

  // If the TCP connection was left open accidentally, close it
  if(client.connected())
  {
    client.stop();
  }

  // If the buffer_full_semaphore is set to FRAMEBUFFER_TOO_SMALL then there was not 
  // enough space in the supplied framebuffer to capture a full image
  if(framebuffer_0.buffer_full_semaphore == FRAMEBUFFER_TOO_SMALL)
  {
    g_frames_captured = 0;
    framebuffer_0.buffer_full_semaphore = FRAMEBUFFER_READY;
    led_timer = 5000;
  }

  // When an image is captured, open a connection to ServerIP on ServerPort and send the raw jpeg data
  if(framebuffer_0.buffer_full_semaphore == FRAMEBUFFER_DONE)
  {
    g_frames_captured++;

    client.connect(ServerIP, ServerPort);
    if(client.connected())
    {
      client.write(framebuffer_0.p_framebuffer, framebuffer_0.size);
    }
    
    framebuffer_0.buffer_full_semaphore = FRAMEBUFFER_READY;
  }

  // Code to handle the led timer
  if(led_timer > 0)
  {
    digitalWrite(A5, HIGH);
    led_timer--;
  }
  else
  {
    digitalWrite(A5, LOW);
  }
}

// take_picture signals the uvc driver to start taking a picture with the given paramters
int take_picture(String args)
{
  uvc->take_picture(&framebuffer_0, UVC_VS_FORMAT_MJPEG, 480, 360);
  return 0;
}

int led_set(String cycles)
{
  led_timer = atoi(cycles);
  if(led_timer < 0)
  {
    led_timer = 0;
    return -1;
  }
  return 0;
}